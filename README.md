# idle-breakout

A idle breakout game developed using pixi.js and phaser.js to learn about game dev and javascript physics engines. It's playable but still needs a lot of work [https://chriskingwebdev.gitlab.io/idle-breakout/](https://chriskingwebdev.gitlab.io/idle-breakout/)
